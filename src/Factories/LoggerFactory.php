<?php


namespace Scout\Laravel\BusinessRu\Factories;


use Scout\Laravel\BusinessRu\Config;
use Scout\Laravel\BusinessRu\Logger\ILogger;
use Scout\Laravel\BusinessRu\Logger\Logger;
use Scout\Laravel\BusinessRu\Logger\LoggerMock;

class LoggerFactory
{
    public function make(Config $config): ILogger
    {
        return $config->isLogging() ? new Logger($config) : new LoggerMock;
    }
}