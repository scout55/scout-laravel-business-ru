<?php


namespace Scout\Laravel\BusinessRu\Factories;


use Scout\Laravel\BusinessRu\Mocks\FailMock;
use Scout\Laravel\BusinessRu\Mocks\SuccessMock;
use Scout\Laravel\BusinessRu\OpenApiException;
use Scout\Laravel\BusinessRu\SDK\ISDK;
use Scout\Laravel\BusinessRu\SDK\SDK;

class SDKFactory
{
    private ConfigFactory $config;
    private LoggerFactory $logger;

    public function __construct(ConfigFactory $config, LoggerFactory $logger)
    {
        $this->config = $config;
        $this->logger = $logger;
    }

    /**
     * @param string $name
     * @return ISDK
     * @throws OpenApiException
     */
    public function client(string $name = 'default'): ISDK
    {
        $config = $this->config->make($name);
        $logger = $this->logger->make($config);


        switch (true) {

            case $config->isMockSuccess():
                return new SuccessMock($config, $logger);

            case $config->isMockFail():
                return new FailMock;

            case $config->isProduction():
                return new SDK($config, $logger);

            default:
                throw new OpenApiException('неизвестный env');

        }
    }
}