<?php


namespace Scout\Laravel\BusinessRu\Factories;


use Exception;
use Scout\Laravel\BusinessRu\Config;

class ConfigFactory
{
    /**
     * @param string $name
     * @return Config
     * @throws Exception
     */
    public function make(string $name = 'default'): Config
    {
        return new Config($name);
    }
}