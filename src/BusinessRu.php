<?php


namespace Scout\Laravel\BusinessRu;

use Illuminate\Support\Facades\Facade;
use Scout\Laravel\BusinessRu\SDK\ISDK;

/**
 * Class BusinessRu
 * @package Scout\Laravel\BusinessRu
 *
 * @method static ISDK client(?string $name = 'default')
 *
 * @see \Scout\Laravel\BusinessRu\SDK\ISDK
 * @see \Scout\Laravel\BusinessRu\Factories\SDKFactory
 * @see \Scout\Laravel\BusinessRu\SDK\SDK - основной клиент
 * @see \Scout\Laravel\BusinessRu\Mocks\SuccessMock
 * @see \Scout\Laravel\BusinessRu\Mocks\FailMock
 */
class BusinessRu extends Facade
{
    /**
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return "scout.business-ru";
    }
}
