<?php


namespace Scout\Laravel\BusinessRu\Entity;


use Scout\Laravel\BusinessRu\OpenApiException;

class Bill implements \JsonSerializable
{
    /** @var string Автор смены */
    private string $author;
    /** @var string Почта для чека */
    private string $email;
    /** @var string Номер заказа в нашей системе */
    private string $order_number;
    /** @var int Сумма наличными */
    private int $cash;
    /** @var int Сумма безналичных */
    private int $cashless;
    /** @var Good[] Товары */
    private array $goods;

    /**
     * Bill constructor.
     * @param string $author
     * @param string $email
     * @param string $order_number
     * @param int $cash_kopecks
     * @param int $cashless_kopecks
     * @param Good[] $goods
     * @throws OpenApiException
     */
    public function __construct(string $author, string $email, string $order_number, int $cash_kopecks, int $cashless_kopecks, array $goods)
    {
        $this->email = $email;
        $this->order_number = $order_number;
        $this->cash = $cash_kopecks;
        $this->cashless = $cashless_kopecks;
        $this->goods = $goods;
        $this->author = $author;

        if (empty($goods)) {
            throw new OpenApiException("Список товаров для чека не может быть пустым");
        }
    }

    /**
     * @return string
     */
    public function getAuthor(): string
    {
        return $this->author;
    }

    /**
     * @param string $author
     * @return Bill
     */
    public function setAuthor(string $author): Bill
    {
        $this->author = $author;
        return $this;
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @param string $email
     * @return Bill
     */
    public function setEmail(string $email): Bill
    {
        $this->email = $email;
        return $this;
    }

    /**
     * @return string
     */
    public function getOrderNumber(): string
    {
        return $this->order_number;
    }

    /**
     * @param string $order_number
     * @return Bill
     */
    public function setOrderNumber(string $order_number): Bill
    {
        $this->order_number = $order_number;
        return $this;
    }

    /**
     * @return float - Рубли
     */
    public function getCash(): float
    {
        return $this->cash / 100;
    }

    /**
     * @param int $cash
     */
    public function setCash(int $cash): void
    {
        $this->cash = $cash;
    }

    /**
     * @return float - Рубли
     */
    public function getCashless(): float
    {
        return $this->cashless / 100;
    }

    /**
     * @param int $cashless
     */
    public function setCashless(int $cashless): void
    {
        $this->cashless = $cashless;
    }

    /**
     * @return Good[]
     */
    public function getGoods(): array
    {
        return $this->goods;
    }

    /**
     * @param Good[] $goods
     * @return Bill
     */
    public function setGoods(array $goods): Bill
    {
        $this->goods = $goods;
        return $this;
    }

    public function jsonSerialize()
    {
        return [
            "author" => $this->getAuthor(),
            "email" => $this->getEmail(),
            "order_number" => $this->getOrderNumber(),
            "cash" => $this->getCash(),
            "cashless" => $this->getCashless(),
            "goods" => $this->getGoods(),
        ];
    }
}
