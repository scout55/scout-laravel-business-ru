<?php

namespace Scout\Laravel\BusinessRu\Entity;


class Command implements \JsonSerializable
{
    private int $id;

    /**
     * Command constructor.
     * @param int $id
     */
    public function __construct(int $id)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    public function jsonSerialize()
    {
        return [
            "id" => $this->getId()
        ];
    }
}
