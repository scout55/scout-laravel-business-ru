<?php


namespace Scout\Laravel\BusinessRu\Entity;


use JsonSerializable;

class Good implements JsonSerializable
{
    public const PAYMENT_MODE_PREPAY_FULL = 1;
    public const PAYMENT_MODE_PREPAY_PART = 2;
    public const PAYMENT_MODE_ADVANCE = 3;
    public const PAYMENT_MODE_FULL = 4;


    /** @var int Кол-во штук */
    private int $count;
    /** @var int Конечная сумма одной единицы. С учетом всех скидок/акций */
    private int $price;
    /** @var int Сумма count*price */
    private int $sum;
    /** @var string Название товара */
    private string $name;
    /** @var int Значение ставки НДС */
    private int $nds_value;
    /** @var int Признак способа расчета */
    private int $payment_mode;
    /** @var bool Применять ли НДС ставку */
    private bool $nds_not_apply;

    /**
     * Good constructor.
     * @param int $count
     * @param int $price_in_kopecks
     * @param string $name
     * @param int $nds_value
     * @param int $payment_mode
     */
    public function __construct(int $count, int $price_in_kopecks, string $name, int $nds_value, int $payment_mode)
    {
        $this->count = $count;
        $this->price = $price_in_kopecks;
        $this->sum = $count * $price_in_kopecks;
        $this->name = $name;
        $this->nds_value = $nds_value;
        $this->payment_mode = $payment_mode;
        $this->nds_not_apply = $nds_value <= 0;
    }

    /**
     * @return int
     */
    public function getCount(): int
    {
        return $this->count;
    }

    /**
     * @param int $count
     * @return Good
     */
    public function setCount(int $count): Good
    {
        $this->count = $count;
        return $this;
    }

    /**
     * @return float - Рубли
     */
    public function getPrice(): float
    {
        return $this->price / 100;
    }

    /**
     * @param int $kopecks
     */
    public function setPrice(int $kopecks): void
    {
        $this->price = $kopecks;
    }

    /**
     * @return float - Рубли
     */
    public function getSum(): float
    {
        return $this->sum / 100;
    }

    /**
     * @param int $kopecks
     */
    public function setSum(int $kopecks): void
    {
        $this->sum = $kopecks;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return Good
     */
    public function setName(string $name): Good
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return int
     */
    public function getNdsValue(): int
    {
        return $this->nds_value;
    }

    /**
     * @param int $nds_value
     * @return Good
     */
    public function setNdsValue(int $nds_value): Good
    {
        $this->nds_value = $nds_value;
        return $this;
    }

    /**
     * @return int
     */
    public function getPaymentMode(): int
    {
        return $this->payment_mode;
    }

    /**
     * @param int $payment_mode
     * @return Good
     */
    public function setPaymentMode(int $payment_mode): Good
    {
        $this->payment_mode = $payment_mode;
        return $this;
    }

    /**
     * @return bool
     */
    public function isNdsNotApply(): bool
    {
        return $this->nds_not_apply;
    }

    /**
     * @param bool $nds_not_apply
     * @return Good
     */
    public function setNdsNotApply(bool $nds_not_apply): Good
    {
        $this->nds_not_apply = $nds_not_apply;
        return $this;
    }

    public function jsonSerialize()
    {
        return [
            "count" => $this->getCount(),
            "price" => $this->getPrice(),
            "sum" => $this->getSum(),
            "name" => $this->getName(),
            "nds_value" => $this->getNdsValue(),
            "payment_mode" => $this->getPaymentMode(),
            "nds_not_apply" => $this->isNdsNotApply(),
        ];
    }
}
