<?php


namespace Scout\Laravel\BusinessRu\Logger;


interface ILogger
{
    public function log(string $msg);
}