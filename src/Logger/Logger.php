<?php


namespace Scout\Laravel\BusinessRu\Logger;


use Illuminate\Support\Facades\Log;
use Scout\Laravel\BusinessRu\Config;

class Logger implements ILogger
{
    /** @var Config $config */
    private Config $config;

    public function __construct(Config $config)
    {
        $this->config = $config;
    }

    public function log(string $msg)
    {
        $key = $this->config->getKey();
        $code = $this->getEnvCode();
        Log::info("[scout.business-ru.$key.$code]: $msg");
    }

    private function getEnvCode(): string
    {
        switch (true) {
            case $this->config->isProduction():
                return "prod";
            case $this->config->isMockSuccess():
                return "mock-success";
            case $this->config->isMockFail():
                return "mock-fail";
            default:
                return "unknown";
        }
    }
}