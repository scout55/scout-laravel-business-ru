<?php


namespace Scout\Laravel\BusinessRu\Mocks;


use Scout\Laravel\BusinessRu\Config;
use Scout\Laravel\BusinessRu\Entity\Bill;
use Scout\Laravel\BusinessRu\Entity\Command;
use Scout\Laravel\BusinessRu\Logger\ILogger;
use Scout\Laravel\BusinessRu\SDK\ISDK;

class SuccessMock implements ISDK
{
    private Config $config;
    private ILogger $logger;

    public function __construct(Config $config, ILogger $logger)
    {
        $this->config = $config;
        $this->logger = $logger;
    }

    public function getToken()
    {
        return "110e7894-q34f-egd1-3f48-dcf895d5cds4";
    }

    public function openShift(): Command
    {
        $this->logger->log("Открываю смену");

        return new Command(rand(1000000, 9999999));
    }

    public function closeShift(): Command
    {
        $this->logger->log("закрываю смену");

        return new Command(rand(1000000, 9999999));
    }

    public function printBill(Bill $bill): Command
    {
        $this->logger->log("Отправляю чек " . json_encode($bill, JSON_UNESCAPED_UNICODE));

        return new Command(rand(1000000, 9999999));
    }

    public function printRefundBill(Bill $bill): Command
    {
        $this->logger->log("Отправляю чек возврата " . json_encode($bill, JSON_UNESCAPED_UNICODE));

        return new Command(rand(1000000, 9999999));
    }

    public function getSystemStatus(): array
    {
        return [
            "date_last_connect_app" => "12.06.2017 20:16:00",
            "date_last_connect_ofd" => "12.06.2017 20:16:00",
            "printer_status" => 1,
            "app_name" => "Онлайн-касса",
            "app_version" => "1.0.0",
            "os_name" => "Windows 10",
            "pc_name" => "Work",
            "printer_name" => "Атол 30Ф"
        ];
    }
}