<?php


namespace Scout\Laravel\BusinessRu\Mocks;


use Scout\Laravel\BusinessRu\Entity\Bill;
use Scout\Laravel\BusinessRu\Entity\Command;
use Scout\Laravel\BusinessRu\OpenApiException;
use Scout\Laravel\BusinessRu\SDK\ISDK;

class FailMock implements ISDK
{
    public function getToken()
    {
        throw new OpenApiException('MOCK exception');
    }

    public function openShift(): Command
    {
        throw new OpenApiException('MOCK exception');
    }

    public function closeShift(): Command
    {
        throw new OpenApiException('MOCK exception');
    }

    public function printBill(Bill $bill): Command
    {
        throw new OpenApiException('MOCK exception');
    }

    public function printRefundBill(Bill $bill): Command
    {
        throw new OpenApiException('MOCK exception');
    }

    public function getSystemStatus(): array
    {
        throw new OpenApiException('[TEST] exception');
    }
}