<?php

namespace Scout\Laravel\BusinessRu;

use Illuminate\Foundation\Application;
use Scout\Laravel\BusinessRu\Factories\ConfigFactory;
use Scout\Laravel\BusinessRu\Factories\LoggerFactory;
use Scout\Laravel\BusinessRu\Factories\SDKFactory;

/**
 * Class ServiceProvider
 * @package Scout\Laravel\BusinessRu
 */
class ServiceProvider extends \Illuminate\Support\ServiceProvider
{

    public function boot()
    {
    }

    /**
     * Register application class instance
     */
    public function register(): void
    {
        $this->app->singleton("scout.business-ru", function (Application $app) {
            return new SDKFactory(new ConfigFactory, new LoggerFactory);
        });

    }

}
