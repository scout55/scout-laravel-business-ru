<?php


namespace Scout\Laravel\BusinessRu;


use Exception;

class Config
{
    public const ENV_MOCK_SUCCESS = 'mock-success';
    public const ENV_MOCK_FAIL = 'mock-fail';
    public const ENV_PRODUCTION = 'production';

    private const DEFAULT_URL = "https://check.business.ru/open-api/v1";

    /** @var string */
    private $key;
    /** @var string */
    private $env;
    /** @var string */
    private $app_id;
    /** @var string */
    private $secret_key;
    /** @var string */
    private $url;
    /** @var bool */
    private $logging;


    public function __construct(?string $key = 'default')
    {
        $options = config("services.scout.business-ru.$key");
        $this->key = $key;

        try {

            $this->env = strtolower($options["env"]);
            $this->app_id = (string)$options["app_id"];
            $this->secret_key = (string)$options["secret_key"];
            $this->url = (string)$options["url"] ?? self::DEFAULT_URL;
            if (isset($options["logging"])) {
                $this->logging = filter_var($options["logging"], FILTER_VALIDATE_BOOL);
            } else {
                $this->logging = true;
            }

        } catch (Exception $e) {

            throw new \Exception("Внимание! Неверная настройка конфига в scout/business-ru. Ключ [$key] \n" . $e->getMessage());

        }
    }

    public function getKey(): string
    {
        return (string)$this->key;
    }

    public function isMockSuccess(): bool
    {
        return $this->env === self::ENV_MOCK_SUCCESS;
    }

    public function isMockFail(): bool
    {
        return $this->env === self::ENV_MOCK_FAIL;
    }

    public function isProduction(): bool
    {
        return $this->env === self::ENV_PRODUCTION;
    }

    public function isLogging(): bool
    {
        return (bool)$this->logging;
    }

    /**
     * @return string
     */
    public function getAppId(): string
    {
        return $this->app_id;
    }

    /**
     * @return string
     */
    public function getSecretKey(): string
    {
        return $this->secret_key;
    }

    /**
     * @param string|null $path
     * @return string
     */
    public function getUrl(?string $path = ""): string
    {
        if (strlen($path) > 0 && $path[0] == "/") {
            $path = substr($path, 1);
        }

        return $this->url . "/$path";
    }
}
