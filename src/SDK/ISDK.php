<?php

namespace Scout\Laravel\BusinessRu\SDK;

use Scout\Laravel\BusinessRu\Entity\Bill;
use Scout\Laravel\BusinessRu\Entity\Command;

interface ISDK
{

    public function getToken();

    /**
     * Открыть смену
     *
     * @return Command
     */
    public function openShift(): Command;

    /**
     * Закрыть смену
     *
     * @return Command
     */
    public function closeShift(): Command;

    /**
     * Напечатать чек
     *
     * @param Bill $bill
     * @return Command
     */
    public function printBill(Bill $bill): Command;

    /**
     * Напечатать чек возврата
     *
     * @param Bill $bill
     * @return Command
     */
    public function printRefundBill(Bill $bill): Command;


    /**
     * Получить статус системы
     *
     * @return array
     */
    public function getSystemStatus(): array;
}