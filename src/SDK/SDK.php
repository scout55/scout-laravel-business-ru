<?php

namespace Scout\Laravel\BusinessRu\SDK;

use Illuminate\Support\Facades\Log;
use Scout\Laravel\BusinessRu\Config;
use Scout\Laravel\BusinessRu\Entity\Bill;
use Scout\Laravel\BusinessRu\Entity\Command;
use Scout\Laravel\BusinessRu\Logger\ILogger;
use Scout\Laravel\BusinessRu\OpenApiException;

class SDK implements ISDK
{
    const METHOD_GET = 'get';
    const METHOD_POST = 'post';

    const TYPE_PRINT_CHECK = 'printCheck';
    const TYPE_PRINT_PURCHASE_RETURN = 'printPurchaseReturn';
    const TYPE_PRINT_CASH_IN = 'printCashIn';
    const TYPE_PRINT_CASH_OUT = 'printCashOut';
    const TYPE_PRINT_REPORT = 'printReport';
    const TYPE_PRINT_CHECK_CORRECTION = 'printCheckCorrection';
    const TYPE_CLOSE_SHIFT = 'closeShift';
    const TYPE_OPEN_SHIFT = 'openShift';

    const PATH_TOKEN = "Token";
    const PATH_SYSTEM_STATUS = "StateSystem";
    const PATH_COMMAND = "Command";

    private $token;
    private Config $config;
    private ILogger $logger;

    public function __construct(Config $config, ILogger $logger)
    {
        $this->config = $config;
        $this->token = false;
        $this->logger = $logger;
    }

    /*--------------------------
     * GETTERS
     *-------------------------*/

    public function getToken()
    {
        if ($this->token) {
            return $this->token;
        }

        $data = [
            "nonce" => $this->getNonce(),
            "app_id" => $this->config->getAppId()
        ];

        $response = $this->sendRequest(
            self::METHOD_GET,
            self::PATH_TOKEN,
            $data
        );

        return $this->token = $this->parseToken($response);
    }

    /*--------------------------
     * API
     *-------------------------*/

    public function openShift(): Command
    {
        $data = [
            "command" => [
                "report_type" => false,
                "author" => "name" //TODO: make name
            ]
        ];


        $settings = $this->getRequestSettings(self::TYPE_OPEN_SHIFT);
        $body = array_merge($data, $settings);


        $response = $this->sendRequest(
            self::METHOD_POST,
            self::PATH_COMMAND,
            $body
        );

        return $this->parseCommand($response);
    }

    public function closeShift(): Command
    {
        $data = [
            "command" => [
                "report_type" => false,
                "author" => "name" //TODO: make name
            ]
        ];


        $settings = $this->getRequestSettings(self::TYPE_CLOSE_SHIFT);
        $body = array_merge($data, $settings);

        $response = $this->sendRequest(
            self::METHOD_POST,
            self::PATH_COMMAND,
            $body
        );

        return $this->parseCommand($response);
    }

    public function printBill(Bill $bill): Command
    {
        $data = $this->convertBill($bill);
        $settings = $this->getRequestSettings(self::TYPE_PRINT_CHECK);

        $body = array_merge($data, $settings);

        $response = $this->sendRequest(
            self::METHOD_POST,
            self::PATH_COMMAND,
            $body
        );

        return $this->parseCommand($response);
    }

    public function printRefundBill(Bill $bill): Command
    {
        $data = $this->convertBill($bill);
        $settings = $this->getRequestSettings(self::TYPE_PRINT_PURCHASE_RETURN);

        $body = array_merge($data, $settings);

        $response = $this->sendRequest(
            self::METHOD_POST,
            self::PATH_COMMAND,
            $body
        );

        return $this->parseCommand($response);
    }

    public function getSystemStatus(): array
    {
        return $this->sendRequest(
            self::METHOD_GET,
            self::PATH_SYSTEM_STATUS,
            $this->getRequestSettings()
        );
    }

    /*--------------------------
     * METHODS
     *-------------------------*/

    private function convertBill(Bill $bill): array
    {
        $goods = [];

        foreach ($bill->getGoods() as $good) {
            $goods []= [
                "count" => $good->getCount(),
                "price" => $good->getPrice(),
                "sum" => $good->getSum(),
                "name" => $good->getName(),
                "nds_value" => $good->getNdsValue(),
                "payment_mode" => $good->getPaymentMode(),
                "nds_not_apply" => $good->isNdsNotApply(),
            ];
        }


        return [
            "command" => [
                "author" => $bill->getAuthor(),
                "smsEmail54FZ" => $bill->getEmail(),
                "c_num" => $bill->getOrderNumber(),
                "payed_cash" => $bill->getCash(),
                "payed_cashless" => $bill->getCashless(),
                "goods" => $goods
            ]
        ];
    }

    private function parseCommand(array $response): Command
    {
        if (isset($response["command_id"])) {
            return new Command($response['command_id']);
        }

        throw new OpenApiException("Не смог распрасить команду. Response: " . json_encode($response, JSON_UNESCAPED_UNICODE));
    }


    private function parseToken(array $response): string
    {
        if (isset($response["token"])) {
            return (string)$response['token'];
        }

        throw new OpenApiException("Не смог получить токен. Response: " . json_encode($response, JSON_UNESCAPED_UNICODE));
    }

    private function sendRequest($method, $path, $params): array
    {
        ksort($params);

        $url = $this->config->getUrl($path);

        $this->logger->log("Отправляю на ($url): " . json_encode($params, JSON_UNESCAPED_UNICODE));

        $cURL = curl_init();

        if (strtolower($method) === self::METHOD_GET) {
            curl_setopt_array(
                $cURL,
                [
                    CURLOPT_URL => $url . "?" . http_build_query($params),
                    CURLOPT_CUSTOMREQUEST => "GET",
                    CURLOPT_HTTPHEADER => [
                        "sign: " . $this->getSign($params),
                    ]
                ]
            );
        } else {
            curl_setopt_array(
                $cURL,
                [
                    CURLOPT_URL => $url,
                    CURLOPT_CUSTOMREQUEST => "POST",
                    CURLOPT_POSTFIELDS => json_encode($params, JSON_UNESCAPED_UNICODE),
                    CURLOPT_HTTPHEADER => [
                        "Content-Type: application/json; charset=utf-8",
                        "accept: application/json",
                        "Content-Length: " . strlen(json_encode($params, JSON_UNESCAPED_UNICODE)),
                        "sign: " . $this->getSign($params),
                    ]
                ]
            );
        }

        curl_setopt_array(
            $cURL,
            [
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_CONNECTTIMEOUT => 5,
            ]
        );

        $result = curl_exec($cURL);

        $this->logger->log("Получил: $result");

        return json_decode($result, true) ?: [];
    }

    private function getNonce()
    {
        try {
            $salt = (int)(now()->format('YmdHis') . random_int(10, 99));
        } catch (\Exception $e) {
            $salt = rand(1000000, 9999999);
        }

        return "salt_SBR" . $salt;
    }

    private function getSign($params)
    {
        return md5(
            json_encode(
                $params,
                JSON_UNESCAPED_UNICODE
            ) . $this->config->getSecretKey()
        );
    }

    private function getRequestSettings(?string $type = null): array
    {
        $data = [
            "app_id" => $this->config->getAppID(),
            "nonce" => $this->getNonce(),
            "token" => $this->getToken(),
        ];

        if (!empty($type)) {
            $data["type"] = $type;
        }

        return $data;
    }
}
