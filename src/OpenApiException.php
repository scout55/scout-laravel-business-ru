<?php


namespace Scout\Laravel\BusinessRu;



use Throwable;

class OpenApiException extends \Exception
{
    public function __construct($message = "", $code = 0, Throwable $previous = null)
    {
        parent::__construct("[scout.business-ru]: ".$message, $code, $previous);
    }
}
