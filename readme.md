Клиент для API business.ru
----

- Laravel пакет для версии >7.0
- PHP 7.4

-----------

## Установка

1. Добавить репозиторий в composer.json
    1. ```composer config repositories.scout git-bitbucket https://bitbucket.org/scout55/scout-laravel-business-ru.git```
2. Добавить пакет в зависимости
    1. ```composer require scout/laravel-business-ru ```

---

## Настройка

1. Добавить настройки интеграций в ```config/services.php```
2. Выполнить ``php artisan optimize``

|Значение|Тип|По-умолчанию|Описание|
|---|---|---|---|
|services.scout.business-ru.X|string|default|X - название клиента|
|services.scout.business-ru.X.env|string| |production, mock-success, mock-fail|
|services.scout.business-ru.X.app_id|string| |app_id интеграции|
|services.scout.business-ru.X.secret_key|string| |secret_key интеграции|
|services.scout.business-ru.X.url|string|https://check.business.ru/open-api/v1|Путь до сервиса|
|services.scout.business-ru.X.logging|bool|true|Выводить информацию в лог|

#### Пример конфига

```php
return [
    // ...
    'scout' => [
        'business-ru' => [
            'default' => [
            'env' => 'mock-success',
            'app_id' => 'mock-app-id',
            'secret_key' => 'mock-secret-key',
            'url' => 'mock-base-url',
            'logging' => true
            ]
        ]
    ],
    // ...
];
```

---

## Использование

```php
use Scout\Laravel\BusinessRu\BusinessRu;

BusinessRu::client()->openShift();

BusinessRu::client('my-config-key')->openShift();

```